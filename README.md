Remote control for kitchen lighting.

This application uses the library Android Holo ColorPicker (https://github.com/LarsWerkman/HoloColorPicker).
This library is available under the Apache 2.0 license, which can be obtained from http://www.apache.org/licenses/LICENSE-2.0.