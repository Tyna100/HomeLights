package com.example.homelights.main;

import android.content.Intent;
import android.speech.RecognizerIntent;
import android.widget.SeekBar;

import androidx.annotation.Nullable;

import com.larswerkman.holocolorpicker.ColorPicker;

import java.util.List;

public class VoiceCommands {

    public static byte[] recognizeVoiceCommand(@Nullable Intent data, ColorPicker colorPicker, SeekBar whiteValueSeekBar){
        List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
        String result = results.get(0).toUpperCase();

        if(result.contains("LIGHTS ON") || result.contains("LIGHTS OFF")){
            return LightControl.toggleRequest();
        }
        else if(result.toUpperCase().contains("LIGHTS WHITE")){
            return LightControl.refreshLightsRequest(ColorOption.WHITE,colorPicker,whiteValueSeekBar);
        }
        else if(result.toUpperCase().contains("LIGHTS RED")){
            return LightControl.refreshLightsRequest(ColorOption.COLOR_RED,colorPicker,whiteValueSeekBar);
        }
        else if(result.toUpperCase().contains("LIGHTS BLUE")){
            return LightControl.refreshLightsRequest(ColorOption.COLOR_BLUE,colorPicker,whiteValueSeekBar);
        }
        else if(result.toUpperCase().contains("LIGHTS GREEN")){
            return LightControl.refreshLightsRequest(ColorOption.COLOR_GREEN,colorPicker,whiteValueSeekBar);
        }
        else if(result.toUpperCase().contains("LIGHTS YELLOW")){
            return LightControl.refreshLightsRequest(ColorOption.COLOR_YELLOW,colorPicker,whiteValueSeekBar);
        }
        else if(result.toUpperCase().contains("LIGHTS MAGENTA")){
            return LightControl.refreshLightsRequest(ColorOption.COLOR_MAGENTA,colorPicker,whiteValueSeekBar);
        }
        else {
            return LightControl.refreshLightsRequest(ColorOption.WHITE,colorPicker,whiteValueSeekBar);
        }
    }

}
