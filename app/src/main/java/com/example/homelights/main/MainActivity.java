package com.example.homelights.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homelights.R;
import com.example.homelights.settings.ConnectionSettings;
import com.example.homelights.settings.SettingsActivity;
import com.google.android.material.button.MaterialButton;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.ValueBar;

import java.lang.ref.WeakReference;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_SPEECH_RECOGNIZER = 3000;
    private ColorOption colorOption;
    private ColorPicker colorPicker;
    private SeekBar whiteValueSeekBar;
    private MaterialButton refreshButton;
    private ValueBar valueBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final MaterialButton turnOnOffButton = findViewById(R.id.lights_on_off);
        final RadioButton changeColorButton = findViewById(R.id.color_radio_button);
        final RadioButton whiteButton = findViewById(R.id.white_radio_button);
        final MaterialButton voiceButton = findViewById(R.id.voice);

        colorPicker = findViewById(R.id.color_picker);
        valueBar = findViewById(R.id.value_bar);
        refreshButton = findViewById(R.id.refresh);
        whiteValueSeekBar = findViewById(R.id.white_value);

        colorOption = ColorOption.WHITE;

        ConnectionSettings.initConnectionSettings(this);

//        rooms
        List<Room> rooms = Arrays.asList(
                new Room("Kitchen"),
                new Room("Bathroom"));
        final RecyclerView roomsRecyclerView = findViewById(R.id.room_recycler_view);
        roomsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager roomsLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        roomsRecyclerView.setLayoutManager(roomsLayoutManager);
        RoomViewAdapter adapter = new RoomViewAdapter(rooms);
        roomsRecyclerView.setAdapter(adapter);

        colorPicker.addValueBar(valueBar);

        refreshButton.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        turnOnOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LedTask(MainActivity.this).execute(LightControl.toggleRequest());
            }
        });

        colorPicker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                refreshButton.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
        });

        changeColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorOption = ColorOption.COLOR;
                refreshButton.getBackground().setColorFilter(colorPicker.getColor(), PorterDuff.Mode.SRC_ATOP);
                colorPicker.setVisibility(View.VISIBLE);
                valueBar.setVisibility(View.VISIBLE);
                whiteValueSeekBar.setVisibility(View.GONE);

            }
        });

        whiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorOption = ColorOption.WHITE;
                refreshButton.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                valueBar.setVisibility(View.GONE);
                whiteValueSeekBar.setVisibility(View.VISIBLE);
                colorPicker.setVisibility(View.GONE);

            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LedTask(MainActivity.this).execute(LightControl.refreshLightsRequest(colorOption,colorPicker,whiteValueSeekBar));
            }
        });

        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSpeechRecognizer();
            }
        });

    }

    private void startSpeechRecognizer(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        startActivityForResult(intent,REQUEST_SPEECH_RECOGNIZER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_SPEECH_RECOGNIZER){
            if(resultCode == RESULT_OK){
                new LedTask(this).execute(VoiceCommands.recognizeVoiceCommand(data,colorPicker,whiteValueSeekBar));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    static class LedTask extends AsyncTask<byte[],String,Boolean>{

        private String ip = ConnectionSettings.getIp();
        private int port = ConnectionSettings.getPort();
        private WeakReference<Activity> activity;

        LedTask(Activity activity){
            this.activity = new WeakReference<>(activity);
        }

        @Override
        protected Boolean doInBackground(byte[]... bytes) {
            byte[] frame = bytes[0];
            try {
                DatagramSocket socket = new DatagramSocket();
                InetSocketAddress address = new InetSocketAddress(ip,port);
                DatagramPacket packet = new DatagramPacket(frame, frame.length,address);
                socket.send(packet);
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(!success) Toast.makeText(activity.get().getApplicationContext(),R.string.connection_failed_message, Toast.LENGTH_SHORT).show();
        }
    }
}
