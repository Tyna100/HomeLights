package com.example.homelights.main;

import android.graphics.Color;
import android.widget.SeekBar;

import com.larswerkman.holocolorpicker.ColorPicker;

public class LightControl {

    public static byte[] toggleRequest(){
        return new byte[]{0x03};
    }

    public static byte[] refreshLightsRequest(ColorOption colorOption, ColorPicker colorPicker, SeekBar whiteValueSeekBar) {

        byte[] colors = new byte[5];

        switch (colorOption) {
            case WHITE:
                int a = whiteValueSeekBar.getProgress();
                colors[0] = 0x04;
                colors[1] = 0;
                colors[2] = 0;
                colors[3] = 0;
                colors[4] = (a < 170 ? (byte) a : (byte) 170);
                break;

            case COLOR:
                changeColor(colorPicker.getColor(), colorPicker,colors);
                break;

            case COLOR_RED:
                changeColor(Color.RED, colorPicker,colors);
                break;

            case COLOR_BLUE:
                changeColor(Color.BLUE, colorPicker,colors);
                break;

            case COLOR_GREEN:
                changeColor(Color.GREEN, colorPicker,colors);
                break;

            case COLOR_YELLOW:
                changeColor(Color.YELLOW, colorPicker,colors);
                break;

            case COLOR_MAGENTA:
                changeColor(Color.MAGENTA, colorPicker,colors);
                break;

        }
        return colors;
    }
        private static void changeColor(int color, ColorPicker colorPicker, byte[] colors){
            int g = Color.green(color);
            int r = Color.red(color);
            int b = Color.blue(color);

            colors[0] = 0x04;
            colors[1] = (byte) g;
            colors[2] = (byte) r;
            colors[3] = (byte) b;
            colors[4] = 0;

            colorPicker.setOldCenterColor(color);

        }
}

