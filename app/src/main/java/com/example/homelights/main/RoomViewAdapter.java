package com.example.homelights.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homelights.R;

import java.util.List;

public class RoomViewAdapter extends RecyclerView.Adapter<RoomViewAdapter.ViewHolder> {

    private List<Room> rooms;

    RoomViewAdapter(List<Room> rooms){
        this.rooms = rooms;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.roomName.setText(rooms.get(position).getRoomName());
    }

    @Override
    public int getItemCount(){return rooms.size();}

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView roomName;
        ViewHolder(@NonNull View itemView){
            super(itemView);
            roomName = itemView.findViewById(R.id.room_name);
        }
    }
    void setNewRooms(List<Room> rooms){this.rooms = rooms;}
}
