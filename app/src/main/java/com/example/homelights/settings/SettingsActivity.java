package com.example.homelights.settings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.homelights.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class SettingsActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final TextInputEditText ipEditText = findViewById(R.id.ip_edit_text);
        final TextInputEditText portEditText = findViewById(R.id.port_edit_text);
        final MaterialButton saveButton = findViewById(R.id.save_settings_button);

        sharedPreferences = getSharedPreferences(ConnectionSettings.PREFERENCES,MODE_PRIVATE);

        ipEditText.setText(sharedPreferences.getString(ConnectionSettings.IP,""));
        if(sharedPreferences.getInt(ConnectionSettings.PORT, 0) == 0){
            portEditText.setText("");
        }
        else {
            portEditText.setText(String.valueOf(sharedPreferences.getInt(ConnectionSettings.PORT,0)));
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ip = ipEditText.getText().toString();
                int port = 0;
                if(!portEditText.getText().toString().isEmpty()){
                    port = Integer.parseInt(portEditText.getText().toString());
                }
                ConnectionSettings.setIp(ip);
                ConnectionSettings.setPort(port);
                if(saveConnectionSettings(ip,port)){
                    Toast.makeText(getApplicationContext(),R.string.connection_settings_saved_successfully,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean saveConnectionSettings(String ip, Integer port){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConnectionSettings.IP, ip);
        editor.putInt(ConnectionSettings.PORT, port);
        return editor.commit();
    }
}
