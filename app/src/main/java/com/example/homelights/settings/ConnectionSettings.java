package com.example.homelights.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.homelights.main.MainActivity;

public class ConnectionSettings {
    public static final String PREFERENCES = "shared_preferences_connection_settings";
    public static final String IP = "shared_preferences_ip";
    public static final String PORT = "shared_preferences_port";
    private static String ip;
    private static int port;

    public static void initConnectionSettings(Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        ip = sharedPreferences.getString(IP,"");
        port = sharedPreferences.getInt(PORT,0);
    }

    public static String getIp() {
        return ip;
    }

    public static int getPort() {
        return port;
    }

    public static void setIp(String ip) {
        ConnectionSettings.ip = ip;
    }

    public static void setPort(int port) {
        ConnectionSettings.port = port;
    }
}
